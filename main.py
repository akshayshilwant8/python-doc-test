"""
This module provides functions for performing various mathematical operations.
"""

def multiply(a, b):
    """
    Multiply two numbers.

    Parameters:
    a (float): The first number to multiply.
    b (float): The second number to multiply.

    Returns:
    float: The product of a and b.
    """
    return a * b

class Circle:
    """
    A class representing a circle.

    Attributes:
    radius (float): The radius of the circle.
    """

    def __init__(self, radius):
        self.radius = radius

    def area(self):
        """
        Calculate the area of the circle.

        Returns:
        float: The area of the circle.
        """
        return 3.14 * self.radius ** 2


multiply(1,2)